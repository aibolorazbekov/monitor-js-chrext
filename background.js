chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  if (
    request &&
    request.action === "createWindow" &&
    request.position === "left"
  ) {
    chrome.windows.create(
      { url: request.link, left: -request.width },
      function () {}
    );
  }

  if (
    request &&
    request.action === "createWindow" &&
    request.position === "right"
  ) {
    chrome.windows.create(
      { url: request.link, left: request.width },
      function () {}
    );
  }

  if (
    request &&
    request.action === "createWindow" &&
    request.position === "top"
  ) {
    chrome.windows.create(
      { url: request.link, top: -request.height },
      function () {}
    );
  }

  if (
    request &&
    request.action === "createWindow" &&
    request.position === "bottom"
  ) {
    chrome.windows.create(
      { url: request.link, top: request.height },
      function () {}
    );
  }
});
