const left_monitors = document.querySelectorAll(".monitor-js-left");
const right_monitors = document.querySelectorAll(".monitor-js-right");
const top_monitors = document.querySelectorAll(".monitor-js-top");
const bottom_monitors = document.querySelectorAll(".monitor-js-bottom");

for (i = 0; i < left_monitors.length; ++i) {
  left_monitors[i].addEventListener("click", function (e) {
    e.preventDefault();
    chrome.runtime.sendMessage(
      {
        action: "createWindow",
        position: "left",
        link: location.origin + this.dataset.link,
        width: window.screen.width,
      },
      function (res) {}
    );
  });
}

for (i = 0; i < right_monitors.length; ++i) {
  right_monitors[i].addEventListener("click", function (e) {
    e.preventDefault();
    chrome.runtime.sendMessage(
      {
        action: "createWindow",
        position: "right",
        link: location.origin + this.dataset.link,
        width: window.screen.width,
      },
      function (res) {}
    );
  });
}

for (i = 0; i < top_monitors.length; ++i) {
  top_monitors[i].addEventListener("click", function (e) {
    e.preventDefault();
    chrome.runtime.sendMessage(
      {
        action: "createWindow",
        position: "top",
        link: location.origin + this.dataset.link,
        height: window.screen.height,
      },
      function (res) {}
    );
  });
}

for (i = 0; i < bottom_monitors.length; ++i) {
  bottom_monitors[i].addEventListener("click", function (e) {
    e.preventDefault();
    chrome.runtime.sendMessage(
      {
        action: "createWindow",
        position: "bottom",
        link: location.origin + this.dataset.link,
        height: window.screen.height,
      },
      function (res) {}
    );
  });
}
