# MonitorJS Chrome Extension

MonitorJS is an extension for chrome that helps you to open web pages in monitors.

  - Open web page in left monitor
  - Open web page in right monitor
  - Open web page in top monitor
  - Open web page in bottom monitor

### Instruction

In order to open web page in monitor add class to DOM and data-link attribute:
```sh
<a class="monitor-js-left" data-link="/link/to/page">Left Montior</a> 
<a class="monitor-js-right" data-link="/link/to/page">Right Montior</a> 
<a class="monitor-js-top" data-link="/link/to/page">Top Montior</a> 
<a class="monitor-js-bottom" data-link="/link/to/page">Bottom Montior</a> 
```

#### Install the extension on your local machine
1. Navigate to `chrome://extensions` in your browser. You can also access this page by clicking on the Chrome menu on the top right side of the Omnibox, hovering over ***More Tools*** and selecting ***Extensions***.
2. Check the box next to ***Developer Mode***.
3. Click ***Load Unpacked Extension*** and select the directory for downloaded extension.
